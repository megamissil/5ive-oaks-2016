<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php
	// If a feature image is set, get the id, so it can be injected as a css background property
	if ( has_post_thumbnail( $post->ID ) ) :
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		$image = $image[0];
		?>

		<img class="sub-bg" src="<?php echo $image ?>" alt="bg">

<?php endif; ?>

   	<div id="single-recipe" class="sub-page" role="main">
      	<section class="page-content">
         	<div class="row">
		        <div class="small-12 columns">
		           <?php while ( have_posts() ) : the_post(); ?>
		             <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
		                 <header>
		                     <h1 class="entry-title"><?php the_title(); ?></h1>
		                 </header>
		                 <div class="sub-content">
		                 	<div class="row">
				                <div class="medium-10 medium-centered text-center columns recipe-excerpt">
									<?php the_excerpt(); ?>
				                </div>
		                    </div>
 		                    <div class="row">
								<div class="medium-4 medium-centered columns recipe-portion">
									<p><?php echo types_render_field( "recipe-portions", array() ) ?></p>
								</div>
							</div>
							<div class="row recipe-content">
								<div class="medium-5 large-4 columns">
									<h5>Ingredients</h5>
									<?php echo types_render_field( "recipe-ingredients", array() ) ?>
								</div>
								<div class="medium-7 large-8 columns">
									<h5>Directions</h5>
									<?php echo types_render_field( "recipe-directions", array() ) ?>
								</div>
		                    </div>

		                    <?php endwhile; wp_reset_query(); ?>

		                 </div>
		             </article>
		        </div>
         	</div>
      	</section>
   	</div>
<?php get_footer();

<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php
	// If a feature image is set, get the id, so it can be injected as a css background property
	if ( has_post_thumbnail( $post->ID ) ) :
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		$image = $image[0];
		?>

		<img class="sub-bg" src="<?php echo $image ?>" alt="bg">

<?php endif; ?>

   	<div id="single-recipe" class="sub-page" role="main">
      	<section class="page-content">
         	<div class="row">
		        <div class="small-12 columns">
		           <?php while ( have_posts() ) : the_post(); ?>
		             <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
		                 <header>
		                     <h1 class="entry-title"><?php the_title(); ?></h1>
		                 </header>
		                 <div class="sub-content">
		                 	<div class="row recipe-item">
		                 		<div class="medium-6 columns recipe-image">
				                    <?php the_post_thumbnail('single-recipe'); ?>
				                </div>
				                <div class="medium-6 columns recipe-content">
									<?php the_excerpt(); ?>
				                </div>
		                    </div>
		                    <div class="row">
		                      <div class="small-12 columns">
		                        <?php the_content(); ?>
		                      </div>
		                    </div>

		                    <?php endwhile; wp_reset_query(); ?>

		                 </div>

		             </article>

		        </div>
         	</div>
      	</section>
   	</div>
<?php get_footer();

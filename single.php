<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="single-post" class="sub-page" role="main">
    <section class="page-content">
        <div class="row">
            <div class="small-12 medium-10 medium-centered columns">
				<?php while ( have_posts() ) : the_post(); ?>
					<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
				        <header>
				            <h1 class="entry-title"><?php the_title(); ?></h1>
				        </header>

						<div class="entry-content sub-content">
							<div class="row">
								<div class="medium-4 columns">
									<?php if ( has_post_thumbnail() ) : the_post_thumbnail(); endif; ?>
								</div>
								<div class="medium-8 columns">
									<?php the_content(); ?>
								</div>
							</div>
						</div>

					</article>
				<?php endwhile;?>
            </div>
        </div>
    </section>
</div>
<?php get_footer();

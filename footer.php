<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>
		<div id="footer-container">
			<section class="top-footer">
				<div class="row">
					<div class="medium-6 columns home-featured-col">
					<h3>Featured Recipe</h3>
						<?php $args = array(
                          	'post_type' => 'recipe',
                          	'posts_per_page' => 1,
                          	'tax_query' => array(
                            array(
                              'taxonomy' => 'category',
                              'field' => 'slug',
                              'terms' => 'featured'
                            )
                          )
                        );
                        $featured = new WP_Query( $args );
                        while( $featured->have_posts() ) : $featured->the_post(); ?>
							<div class="home-featured-post">
								<?php if ( has_post_thumbnail( $post->ID ) ) :
										$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'home-featured' );
										$image = $image[0];
										?>

										<div class="home-featured-img" style="background-image: url('<?php echo $image ?>');"></div>
								<?php endif; ?>
			
								<h5><?php the_title(); ?></h5>
							</div>
						<?php endwhile; wp_reset_query(); ?>
					</div>
					<div class="medium-6 columns home-connected-col">
						<h3>Stay Connected</h3>
						<ul class="social-links">
							<li><a href="https://www.facebook.com/5iveoaks/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
							<li><a href="https://www.instagram.com/5iveoaks/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							<li><a href="mailto:connect@5iveoaks.com" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
						</ul>
						<?php dynamic_sidebar( 'footer-mailchimp' ); ?>
					</div>					
				</div>
			</section>
			<section class="bottom-footer">
				<div class"row">
					<div class="ft-description">
						<?php dynamic_sidebar( 'footer-description' ); ?>
						<p class="copy hide-for-medium">
							Copyright &copy; <?=date('Y'); ?> 5iveOaks. All Rights Reserved<br>
							<a href="<?php echo get_stylesheet_directory_uri(); ?>/assets/files/privacy policy.pdf" target="_blank">Privacy Policy</a><br>
							<span class="moxy"><a href="http://digmoxy.com" target="_blank">Moxy</a></span>
						</p>

						<p class="copy show-for-medium">Copyright &copy; <?=date('Y'); ?> 5iveOaks. All Rights Reserved | <a href="<?php echo get_stylesheet_directory_uri(); ?>/assets/files/privacy policy.pdf" target="_blank">Privacy Policy</a> <span class="moxy"><a href="http://digmoxy.com" target="_blank">Moxy</a></span></p>
					</div>
				</div>
			</section>
		</div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>


<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>

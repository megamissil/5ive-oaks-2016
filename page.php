<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

<?php
  // If a feature image is set, get the id, so it can be injected as a css background property
  if ( has_post_thumbnail( $post->ID ) ) :
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
    $image = $image[0];
    ?>

    <img class="sub-bg" src="<?php echo $image ?>" alt="bg">

<?php endif; ?>

   <div id="page-sub" class="sub-page" role="main">

      <section class="page-content">
         <div class="row">
            <div class="small-12 medium-10 medium-centered columns">
               <?php while ( have_posts() ) : the_post(); ?>
                 <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
                     <header>
                         <h1 class="entry-title"><?php the_title(); ?></h1>
                     </header>
                     <div class="sub-content">
                           <?php the_content(); ?>
                     </div>

                 </article>
               <?php endwhile;?>
            </div>
         </div>
      </section>
   </div>

 <?php get_footer(); ?>

<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- Favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<!-- Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Cabin:400,500,600,700,400italic|Trocchi' rel='stylesheet' type='text/css'>

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<?php include_once("google-analytics.php") ?>

	<?php do_action( 'foundationpress_after_body' ); ?>
	<?php if ( ! is_page('Splash') ) { ?>
		<div class="mobile-top-bar">
			<div class="row">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="mobile-nav-logo">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/5o-logo-1.png" alt="logo">
				</a>

				<div class="mobile-nav-icon">
					<a href="javascript:;"><i class="fa fa-bars"></i></a>
				</div>

				<div class="mobile-nav">
					<i class="fa fa-times close"></i>
					<div class="mobile-nav-links">
						<?php wp_nav_menu( array('menu' => 'Mobile' )); ?>
					</div>
				</div>
			</div>
		</div>

		<?php do_action( 'foundationpress_layout_start' ); ?>

		<header id="masthead" class="site-header" role="banner">
			<nav id="site-navigation" class="main-navigation top-bar" role="navigation">
				<div class="row collapse">
					<div class="medium-5 columns">
						<?php foundationpress_top_bar_l(); ?>
					</div>
					<div class="medium-2 columns">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/5o-logo-1.png" alt="logo">
						</a>
					</div>
					<div class="medium-5 columns">
						<?php foundationpress_top_bar_r(); ?>
					</div>
				</div>
			</nav>
		</header>
	<?php } ?>


	<section class="container">
		<?php do_action( 'foundationpress_after_header' );

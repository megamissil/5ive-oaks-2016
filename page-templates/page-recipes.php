<?php
/*
Template Name: Recipes
*/

 get_header(); ?>

 <?php
   // If a feature image is set, get the id, so it can be injected as a css background property
   if ( has_post_thumbnail( $post->ID ) ) :
     $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
     $image = $image[0];
     ?>

     <img class="sub-bg" src="<?php echo $image ?>" alt="bg">

 <?php endif; ?>

   <div id="page-recipes" class="sub-page" role="main">
      <section class="page-content">
         <div class="row">
            <div class="small-12 columns">
               <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
                  <header>
                      <h1 class="entry-title"><?php the_title(); ?></h1>
                  </header>
                  <div class="sub-content">
                     <?php the_content();

                     $recipes = array ( 'post_type' => 'recipe', 'posts_per_page' => -1 );
                     query_posts($recipes);
                     if ( have_posts() ) : while (have_posts()) : the_post(); ?>
                        <div class="row recipe-item">
                          <div class="medium-4 columns">
                              <a href="<?= get_the_permalink(); ?>">
                                <?php the_post_thumbnail('products-list'); ?>
                              </a>
                          </div>
                          <div class="medium-8 columns">
                            <h4><?php the_title(); ?></h4>
                            <p><?php the_excerpt(); ?></p>
                            <a href="<?= get_the_permalink(); ?>" class="button">Learn More</a>
                          </div>
                        </div>
                        <hr>
                     <?php endwhile; ?>
                     <?php else : ?>
                     <!-- No posts found -->
                     <?php endif; ?>
                  </div>
               </article>
            </div>
         </div>
      </section>
   </div>

 <?php get_footer(); ?>

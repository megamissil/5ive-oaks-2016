<?php
/*
Template Name: Home
*/
get_header(); ?>

<div id="page-home" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>

  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
    <div class="home-slider-container">
      <div class="home-slider">
         <?php $slider = new WP_Query( array( 'post_type' => 'home-slider', 'posts_per_page' => -1 ) );
         while( $slider->have_posts() ) : $slider->the_post(); ?>
           <div>
              <?php echo types_render_field( "home-slider-image", array( "alt" => "home-slider-image" ) ) ?>
           </div>
         <?php endwhile; wp_reset_query(); ?>
      </div>
      <div class="slider-caption top">
         Tastes Like Tradition...
      </div>
      <div class="slider-caption bottom">
         Feels like home.
      </div>
       <div class="arrow"><i class="fa fa-angle-down fa-5x" aria-hidden="true"></i></div>
    </div>
    
    
      <section class="home-content section-3">
        <div class="row">
          <div class="medium-10 medium-centered columns">
            <h2><?php the_title(); ?></h2>
            <?php the_content(); ?>
          </div>
        </div>

      </section>
      
      <div class="parallax-container">
        <section class="parallax section-1">
          <div class="px-slide-1 image" data-type="background" data-speed="3">

          </div>
        </section>
      </div>

      <section class="home-calendar-section">
        <div class="home-calendar-content">
          <h2>Calendar</h2>
          <div class="row">

              <?php echo do_shortcode('[ai1ec view="posterboard"]'); ?>

          </div>
        </div>
      </section>
      
      <div class="parallax-container">
        <section class="parallax section-2">
          <div class="px-slide-2 image" data-type="background" data-speed="4">

          </div>
        </section>
      </div>
  </article>


<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer();

<?php
/*
Template Name: Contact
*/

 get_header(); ?>

 <?php
   // If a feature image is set, get the id, so it can be injected as a css background property
   if ( has_post_thumbnail( $post->ID ) ) :
     $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
     $image = $image[0];
     ?>

       <img class="sub-bg" src="<?php echo $image ?>" alt="bg">

 <?php endif; ?>

    <div id="page-sub" class="sub-page" role="main">
      <section class="page-content">
         <div class="row">
            <div class="small-12 columns">
               <?php while ( have_posts() ) : the_post(); ?>
                 <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
                     <header>
                         <h1 class="entry-title show-for-large"><?php the_title(); ?></h1>
                     </header>
                     <div class="sub-content">
                       <div class="row">
                          <div class="medium-5 columns">
                            <h4><?php the_title(); ?></h4>
                            <?php the_content(); ?>
                          </div>
                          <div class="medium-7 columns">
                              <h4>Calendar</h4>
                              <div class="show-for-medium">
                                <?php echo do_shortcode('[ai1ec view="monthly"]'); ?>
                              </div>
                              <div class="hide-for-medium">
                                <?php echo do_shortcode('[ai1ec view="stream"]'); ?>
                              </div>
                          </div>
                       </div>
                     </div>

                 </article>
               <?php endwhile;?>
            </div>
         </div>
      </section>
   </div>

 <?php get_footer(); ?>

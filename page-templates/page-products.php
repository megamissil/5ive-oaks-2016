<?php
/*
Template Name: Products
*/

get_header(); ?>

<?php
   // If a feature image is set, get the id, so it can be injected as a css background property
   if ( has_post_thumbnail( $post->ID ) ) :
     $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
     $image = $image[0];
     ?>

     <img class="sub-bg" src="<?php echo $image ?>" alt="bg">
<?php endif; ?>

<div id="page-sub" class="sub-page" role="main">
   <section class="page-content">
      <div class="row">
         <div class="small-12 columns">
            <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
               <header>
                   <h1 class="entry-title"><?php the_title(); ?></h1>
               </header>
               <div class="sub-content">
                  <?php the_content(); ?>

                  <div class="row product-item hide-for-medium">
                     <?php $products = array (
                       'post_type' => 'product',
                       'posts_per_page' => -1,
                       'orderby' => 'menu_order',
                       'order' => 'ASC'
                     );
                     query_posts($products);

                     while (have_posts()) : the_post(); ?>
                        <div class="small-12 columns">
                           <div class="product-slider-container">
                              <?php if (types_render_field('product-image', array('output'=>'true'))) { ?>
                                 <div class="product-slider">
                                    <div>
                                       <?php echo types_render_field( "product-image", array( "alt" => "product image", 'separator'=>'</div><div>') ) ?>
                                    </div>
                                 </div>
                              <?php } ?>
                           </div>
                            <div class="product-content">
                               <h5><?php the_title(); ?></h5>
                               <hr>
                               <?php the_content(); ?>
                            </div>
                        </div>
                     <?php endwhile; ?>
                  </div>

                  <div class="row product-item show-for-medium" data-equalizer>
                    <?php $products = array (
                      'post_type' => 'product',
                      'posts_per_page' => -1,
                      'orderby' => 'menu_order',
                      'order' => 'ASC'
                    );
                    query_posts($products);
                    $i = 0;
                    while (have_posts()) : the_post(); ?>
                        <div class="medium-6 columns" id="<?=$i; ?>">
                           <div class="product-slider-container">
                              <?php if (types_render_field('product-image', array('output'=>'true'))) { ?>
                                 <div class="product-slider">
                                    <div>
                                       <?php echo types_render_field( "product-image", array( "alt" => "product image", 'separator'=>'</div><div>') ) ?>
                                    </div>
                                 </div>
                              <?php } ?>
                           </div>

                           <div class="product-content" data-equalizer-watch>
                              <h5><?php the_title(); ?></h5>
                                <hr>
                                <?php the_content(); ?>
                           </div>
                        </div>
                        <?php $i++; ?>
                        <?php if ( ($i == 2) ): ?>
                        </div><!-- /.row -->
                        <div class="row product-item show-for-medium" data-equalizer>
                        <?php endif; ?>

                  <?php endwhile; ?>
                  <?php // wp_reset_query(); ?>
                 </div>
              </div>
            </article>
         </div>
      </div>
   </section>
</div>

 <?php get_footer(); ?>

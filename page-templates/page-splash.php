<?php
/*
Template Name: Splash
*/
?>

<div id="page-splash" role="main">
	<?php get_header(); ?>
	<div class="row">
		<div class="medium-8 medium-centered columns text-center">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/5o-logo-1.png" alt="logo">
			<h3>Tastes Like Tradition, Feels Like Home.</h3>
			<ul class="splash-links">
				<li><a href="https://www.facebook.com/5iveoaks/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
				<li><a href="https://www.instagram.com/5iveoaks/" target="_blank"><i class="fa fa-instagram"></i></a></li>
				<li><a href="mailto:tim@5iveoaks.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
			</ul>
		</div>
	</div>
</div>